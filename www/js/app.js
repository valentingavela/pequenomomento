//--------------------------------------------------------------------------
// manejo de mapas
//--------------------------------------------------------------------------
var mapHandle ;
var currentLat ;
var currentLng ;
var marker ;
var infowindow ;
var geocoder ;
var map ;

// Esto lo voy a quitar cuando los tenga guardados en la base.
var json = [{"id":48,"title":"cordoba","latitude":"-32.08444","longitude":"-64.08444"},
{"id":46,"title":"Salta","latitude":"-65.08444","longitude":"‎-24.08444"},
{"id":44,"title":"Entre Rios","latitude":"-42.491273","longitude":"-60.08444"},
{"id":43,"title":"La Pampa","latitude":"-36.08444","longitude":"-65.08444"},
{"id":39,"title":"Rio Negro","latitude":"-40.08444","longitude":"-65.08444"},
{"id":6,"title":"Chubut","latitude":"-42.08444","longitude":"-67.173409"}];

var iconBase = 'localhost/js/'

var icons = {
  light: {
    icon: iconBase + 'light.png'
  },
  library: {
    icon: iconBase + 'library_maps.png'
  },
  info: {
    icon: iconBase + 'info-i_maps.png'
  }
};


//// Iniciando geocoder
function init_geocoder()
  {
  geocoder = new google.maps.Geocoder();
  if (geocoder==undefined)
    {
    window.alert("No es posible cargar el Geocoder") ;
    }
  else
    {
    localizarme() ;
    }
  }

  function localizarme()
    {
    // Try HTML5 geolocation
    if (navigator.geolocation)
        {
        tryAPIGeolocation
            (
          function(lat,lon)
            {
            setPosition(showmap,'Usted esta aquí',lat,lon,true) ;
            forzarzona(lat,lon) ;
            httpGet("/cgi-bin/distribuidores.pl") ;
            },
          function()
            {
          handleNoGeolocation(true);
            }
          );
        }
      else
        {
        // Browser doesn't support Geolocation
        handleNoGeolocation(false);
        }
    }

  //--------------------------------------------------------------------------
  function handleNoGeolocation(hay)
    {
    if (hay)
            {
  //          window.alert('Su navegador no nos permite la geolocalización. Ingrese su ubicación en el campo "Provincia, localidad, barrio o dirección"') ;
            }
    else
            {
            window.alert('Su navegador no soporta geolocalización. Ingrese su ubicación en el campo "Provincia, localidad, barrio o dirección"') ;
            }
    }
    //--------------------------------------------------------------------------


init_geocoder() ;

 function initMap() {
   map = new google.maps.Map(document.getElementById('map'), {
     center: {lat: -34.60, lng: -58.38},
     zoom: 5,
     styles: [
       {elementType: 'geometry', stylers: [{color: '#242f3e'}]},
       {elementType: 'labels.text.stroke', stylers: [{color: '#242f3e'}]},
       {elementType: 'labels.text.fill', stylers: [{color: '#746855'}]},
       {
         featureType: 'administrative.locality',
         elementType: 'labels.text.fill',
         stylers: [{color: '#d59563'}]
       },
       {
         featureType: 'poi',
         elementType: 'labels.text.fill',
         stylers: [{color: '#d59563'}]
       },
       {
         featureType: 'poi.park',
         elementType: 'geometry',
         stylers: [{color: '#263c3f'}]
       },
       {
         featureType: 'poi.park',
         elementType: 'labels.text.fill',
         stylers: [{color: '#6b9a76'}]
       },
       {
         featureType: 'road',
         elementType: 'geometry',
         stylers: [{color: '#38414e'}]
       },
       {
         featureType: 'road',
         elementType: 'geometry.stroke',
         stylers: [{color: '#212a37'}]
       },
       {
         featureType: 'road',
         elementType: 'labels.text.fill',
         stylers: [{color: '#9ca5b3'}]
       },
       {
         featureType: 'road.highway',
         elementType: 'geometry',
         stylers: [{color: '#746855'}]
       },
       {
         featureType: 'road.highway',
         elementType: 'geometry.stroke',
         stylers: [{color: '#1f2835'}]
       },
       {
         featureType: 'road.highway',
         elementType: 'labels.text.fill',
         stylers: [{color: '#f3d19c'}]
       },
       {
         featureType: 'transit',
         elementType: 'geometry',
         stylers: [{color: '#2f3948'}]
       },
       {
         featureType: 'transit.station',
         elementType: 'labels.text.fill',
         stylers: [{color: '#d59563'}]
       },
       {
         featureType: 'water',
         elementType: 'geometry',
         stylers: [{color: '#17263c'}]
       },
       {
         featureType: 'water',
         elementType: 'labels.text.fill',
         stylers: [{color: '#515c6d'}]
       },
       {
         featureType: 'water',
         elementType: 'labels.text.stroke',
         stylers: [{color: '#17263c'}]
       }
     ]
   });
   infoWindow = new google.maps.InfoWindow;

   // Try HTML5 geolocation.
   if (navigator.geolocation) {
     navigator.geolocation.getCurrentPosition(function(position) {
       var pos = {
         lat: position.coords.latitude,
         lng: position.coords.longitude
       };

       // infoWindow.setPosition(pos);
       // infoWindow.setContent('Location found.');
       // infoWindow.open(map);
       map.setCenter(pos);

       addMarker(pos) ;
       addMarkers() ;

     }, function() {
       handleLocationError(true, infoWindow, map.getCenter());
     });
   } else {
     // Browser doesn't support Geolocation
     handleLocationError(false, infoWindow, map.getCenter());
   }
 }
 //-------------------------------------------------------------------

 function handleLocationError(browserHasGeolocation, infoWindow, pos) {
   infoWindow.setPosition(pos);
   infoWindow.setContent(browserHasGeolocation ?
                         'Error: The Geolocation service failed.' :
                         'Error: Your browser doesn\'t support geolocation.');
   infoWindow.open(map);
 }


 function addMarker(pos){
     var latLng = new google.maps.LatLng(pos);
     var marker = new google.maps.Marker({
       position: latLng,
       map: map,
       icon: 'https://benteveo.com/valentin/rsz_light.png'
     });
 }

 function addMarkers(){
   // Looping through all the entries from the JSON data
    for(var i = 0; i < json.length; i++) {

      // Current object
      var obj = json[i];

      // Adding a new marker for the object
      var marker = new google.maps.Marker({
        position: new google.maps.LatLng(obj.latitude,obj.longitude),
        map: map,
        title: obj.title, // this works, giving the marker a title with the correct title
        // icon: 'js/light.png'
        icon: 'https://benteveo.com/valentin/rsz_light.png'
      });

      // Adding a new info window for the object
      var clicker = addClicker(marker, obj.title);
    } // end loop
 }

 // Adding a new click event listener for the object
 function addClicker(marker, content) {
   google.maps.event.addListener(marker, 'click', function() {

     if (infowindow) {infowindow.close();}
     infowindow = new google.maps.InfoWindow({content: content});
     infowindow.open(map, marker);

   });
 }
